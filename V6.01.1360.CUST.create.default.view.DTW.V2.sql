declare @listOfColumns nvarchar(max)
set @listOfColumns = 'Label_Number';

EXEC dciowner.addIntoCollectionSettings 
	'DTW.V2', 
	'DigitalTwin_Data_View',
	'Default',
	@listOfColumns;