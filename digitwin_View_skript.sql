/*Skript na vyta�en� sloupc� z Warehouse_Packages_View a Warehouse_Position_In_Zone_View 
 jako z�klad pro REST api*/

drop view if exists dciowner.DigitalTwin_Data_View
go

create view dciowner.DigitalTwin_Data_View as
select	wp.Label_Number, 
		wp.Is_Full, 
		wp.Prod_Code, 
		wp.Prod_Desc, 
		wp.Prod_Desc_2,
		wp.Prod_Total_Qty,
		wp.whPosition_Warehouse_ID,
		wp.whPosition_Warehouse_Code,
		wp.whPosition_Position_ID,
		wp.whPosition_Position_ID_Code,
		wp.whPosition_Position_Row,
		wp.whPosition_Position_Column,
		wp.whPosition_Position_Floor,

		wpiz.Zone_ID,
		wpiz.Parent_Zone_ID,
		wpiz.Zone_Status,
		wpiz.Zone_Code,
		wpiz.Zone_Description,
		wpiz.Warehouse_ID,
		wpiz.Warehouse_Code,
		wpiz.Complete_Code,
		wpiz.Position_Code,
		wpiz.Position_Row,
		wpiz.Position_Column,
		wpiz.Position_Floor
from dciowner.Warehouse_Packages_View wp inner join dciowner.Warehouse_Position_In_Zone_View wpiz on
	wp.Current_Warehouse_Position_ID = wpiz.Warehouse_Position_ID

