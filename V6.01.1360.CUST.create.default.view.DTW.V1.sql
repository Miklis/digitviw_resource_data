declare @listOfColumns nvarchar(max)
set @listOfColumns = 'Label_Number
      ,Is_Full
      ,Prod_Code
      ,Prod_Desc
      ,Prod_Desc_2
      ,Prod_Total_Qty
      ,whPosition_Warehouse_ID
      ,whPosition_Warehouse_Code
      ,whPosition_Position_ID
      ,whPosition_Position_ID_Code
      ,whPosition_Position_Row
      ,whPosition_Position_Column
      ,whPosition_Position_Floor
      ,Zone_ID
      ,Parent_Zone_ID
      ,Zone_Status
      ,Zone_Code
      ,Zone_Description
      ,Warehouse_ID
      ,Warehouse_Code
      ,Complete_Code
      ,Position_Code
      ,Position_Row
      ,Position_Column
      ,Position_Floor';

EXEC dciowner.addIntoCollectionSettings 
	'DTW.V1', 
	'DigitalTwin_Data_View',
	'Default',
	@listOfColumns;