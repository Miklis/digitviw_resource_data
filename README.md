Pokud budeš chtít aktualizovat to jaký data chceš tahat, tak spusť všechny sql skripty nad databází dci_AIMDEV_QMIKR622_tst.
Po té co budeš mít skripty tak akorát změníš v nastavení dci C:\user\workspace\AIMDEV_MIKR6220\DCI\Web Content\WEB-INF\collection-columns-cust.xml a tam překopíruješ nastavení
tablů tak jak je to v příslušném souboru v gitu.

Můžeš si v postmanovi vyzkoušet, že se dá brát data z různých modulů. Zatím jsou dva moduly DTW.V1 a DTW.V2. Mezi pohledy se přepíná tak, že v si nastavíš v POSTu search
v body atribut "dataSet" : "DTW.V1" (nebo DTW.V2)